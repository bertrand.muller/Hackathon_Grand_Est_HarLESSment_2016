<?php

namespace app\controller;

use app\model as Model;
use app\model\Event;
use app\view as View;
use \conf as Conf;

class TchatController extends AbstractController
{

    
    function getTchat() {

        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $tchatView = new View\TchatView(null,null);
        $tchatView->renderHeader("<style media=\"screen \" type=\"text/css\">


  
form, p, span {
    margin:0;
    padding:0; }
  
input { font:12px arial; }
  
a {
    color:#0000FF;
    text-decoration:none; }
  
    a:hover { text-decoration:underline; }
  
#wrapper, #loginform {
    margin:0 auto;
    padding-bottom:25px;
    background:#EBF4FB;
    width:504px;
    border:1px solid #ACD8F0;
     text-align: center;}
  
#loginform { padding-top:18px; }
  
    #loginform p { margin: 5px; }
  
#chatbox {
    text-align:left;
    margin:0 auto;
    margin-bottom:25px;
    padding:10px;
    background:#fff;
    height:270px;
    width:430px;
    border:1px solid #ACD8F0;
    overflow:auto; }
  
#usermsg {
    width:395px;
    border:1px solid #ACD8F0; }
  
#submit { width: 60px; }
  
.error { color: #ff0000; }
  
#menu { padding:12.5px 25px 12.5px 25px; }
  
.welcome { float:left; }
  
.logout { float:right; }
  
.msgln { margin:0 0 2px 0; }

</style>
<link href=\"" . $url . "/web/css/tchat.css\" type=\"text/css\" rel=\"stylesheet\" media=\"screen,projection\"/>");
        $tchatView->render(1);
        $tchatView->renderFooter('<script src="' . $url . '/web/js/socket.io-1.4.5.js"></script>
                                  <script src="' . $url . '/web/js/tchat.js"></script>');
    }

}