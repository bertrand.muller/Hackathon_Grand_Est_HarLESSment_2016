<?php
/**
 * Created by PhpStorm.
 * User: Bertrand
 * Date: 05/06/2016
 * Time: 11:43
 */

namespace app\controller;


use app\model\Motcle;
use app\model\Temoignage;
use app\view\TemoignageView;

class TemoignageController extends AbstractController{

    public function getFormTemoignage() {

        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $view = new TemoignageView();
        $view->renderHeader("<link href=\"" . $url . "/web/css/temoignage.css\" type = \"text/css\" rel = \"stylesheet\" media = \"screen,projection\" />
                             <link href=\"" . $url . "/web/css/temoignage.css\" type = \"text/css\" rel = \"stylesheet\" media = \"screen,projection\" />");
        $view->render(1);
        $view->renderFooter("<script>

                $('#tags').hide();

                $('.material-icons').on('click',function() {
                    var chips = document.getElementsByClassName('chip');
                    $('#tags').val('');
                    for(var i=0; i < chips.length; i++) {
                        console.log('chips[' + i + '] = |' + chips[i].textContent + '|');
                        console.log('parent = |' + $(this).parent().text() + '|');
                        if($(this).parent().text() != chips[i].textContent) {
                            if(i!=0) $('#tags').val($('#tags').val()+','+chips[i].textContent.split('close')[0]);
                            else $('#tags').val($('#tags').val()+chips[i].textContent.split('close')[0]);
                        }
                    }
                    console.log($('#tags').val());
                });
                
                
                
            </script>");
    }


    public function saveTemoignage($data) {
        
        if (isset($data['envoyer']) && $data['envoyer'] == 'envoi') {
            $lieu = $data['lieu'];
            $temoignage = $data['texte'];
            $motscles = $data['cles'];

            $tem = new Temoignage();
            $tem->lieu = $lieu;
            $tem->texte = $temoignage;
            $tem->motcles = $motscles;
            $tem->save();
        }

        $cont = new MainController();
        $cont->getHome();

    }


    public function getTemoignages() {

        $view = new TemoignageView();
        $view->renderHeader();
        $view->render(2);
        $view->renderFooter();

    }
    
}