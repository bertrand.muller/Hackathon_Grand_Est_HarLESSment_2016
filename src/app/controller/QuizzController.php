<?php

namespace app\controller;

use app\model as Model;
use app\model\Event;
use app\view as View;
use \conf as Conf;

class QuizzController extends AbstractController {

    function getQuiz() {

        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $quizzView = new View\QuizzView(null,null);
        $quizzView->renderHeader();
        $quizzView->render(1);
        $quizzView->renderFooter('<script type="text/javascript" src="' . $url . '/web/js/jsquiz.js"></script>');
    }

    function getHarcelement() {
		$app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

		$quizzView = new View\QuizzView(null,null);
        $quizzView->renderHeader();
        $quizzView->render(2);
        $quizzView->renderFooter();
    }

    function getTemoin() {
		$app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

		$quizzView = new View\QuizzView(null,null);
        $quizzView->renderHeader();
        $quizzView->render(3);
        $quizzView->renderFooter();

    }

    function getHarcelee() {
		$app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

		$quizzView = new View\QuizzView(null,null);
        $quizzView->renderHeader();
        $quizzView->render(4);
        $quizzView->renderFooter();

    }

     function getPendant() {
		$app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

		$quizzView = new View\QuizzView(null,null);
        $quizzView->renderHeader('<style media="screen " type="text/css"> .switch {

  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}


input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(500px);
  -ms-transform: translateX(100);
  transform: translateX(800);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
});</style>');
        $quizzView->render(5);
        $quizzView->renderFooter();

    }


}