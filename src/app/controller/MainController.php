<?php

namespace app\controller;

use app\view as View;

class MainController extends AbstractController {

	public function getHome() {
		$view = new View\HomeView(null);
		$view->renderHeader();
		$view->render(1);
		$view->renderFooter();
	}

	public function getNotFound()
	{
		$view = new View\HomeView(null);
		$view->renderHeader();
		$view->render(2);
		$view->renderFooter();
	}

}