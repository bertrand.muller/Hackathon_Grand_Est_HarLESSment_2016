<?php
/**
 * Created by PhpStorm.
 * User: froogy444
 * Date: 20/05/16
 * Time: 23:53
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * @var string
     *      Nom de la table associee a la classe Etudiant
     */
    protected $table = 'User';


    /**
     * @var string
     *      Cle primaire de la table etudiant
     */
    protected $primaryKey = 'id';


    /**
     * @var bool
     *      Booleen indiquant l'utilisation de deux colonnes
     *      utilisant des dates lors d'une creation ou d'une mise a jour
     *      dans la base
     */
    public $timestamps = false;
    
}