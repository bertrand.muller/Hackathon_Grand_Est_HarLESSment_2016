<?php
/**
 * Created by PhpStorm.
 * User: Bertrand
 * Date: 04/06/2016
 * Time: 17:34
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    /**
     * @var string
     *      Nom de la table associee a la classe Etudiant
     */
    protected $table = 'Question';


    /**
     * @var string
     *      Cle primaire de la table etudiant
     */
    protected $primaryKey = 'id';


    /**
     * @var bool
     *      Booleen indiquant l'utilisation de deux colonnes
     *      utilisant des dates lors d'une creation ou d'une mise a jour
     *      dans la base
     */
    public $timestamps = false;

}