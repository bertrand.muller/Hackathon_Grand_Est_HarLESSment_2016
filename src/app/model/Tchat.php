<?php
/**
 * Created by PhpStorm.
 * User: Bertrand
 * Date: 05/06/2016
 * Time: 09:36
 */

namespace app\model;


use Illuminate\Database\Eloquent\Model;

class Tchat extends Model{

    /**
     * @var string
     *      Nom de la table associee a la classe Etudiant
     */
    protected $table = 'Tchat';


    /**
     * @var string
     *      Cle primaire de la table etudiant
     */
    protected $primaryKey = array('idDest','idExpe');


    /**
     * @var bool
     *      Booleen indiquant l'utilisation de deux colonnes
     *      utilisant des dates lors d'une creation ou d'une mise a jour
     *      dans la base
     */
    public $timestamps = false;

}