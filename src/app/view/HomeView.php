<?php

namespace app\view;

class HomeView extends AbstractView {

	private $message;

	public function __construct($m) {
		$this->message = $m;
	}

	public function render($selector)
	{
		switch ($selector) {
			case 1:
				echo $this->home();
				break;
			case 2:
				echo $this->notFound();
				break;
			default:
				echo $this->notFound();
				break;
		}
	}

	public function home() {

		$app = \Slim\Slim::getInstance();
		$url = $app->request->getRootUri();
		$url = str_replace("/index.php", "", $url);
		$connect = '';

		/*if (isset($_SESSION['connected'])) {
			if(!$_SESSION['connected']) {
				$connect = '<a href=\"login\" id=\"download-button\" class=\"btn-large waves-effect waves-light red text-darken-1\">Connectez-vous</a>';
			}
		}*/

		$html = "	
			<div class=\"container\">
					<div class=\"section\">
					  <!--   Icon Section   -->
					  <div class=\"row\">
						<div class=\"col s12 m12\">
						  <div class=\"icon-block\">
						  
						  	
				
							<font size='4em'><p class=\"light\" style=\"text-align: justify; display: inline-block;\"'><img class=\"responsive-img\" src=\"".$url."/web/img/tmp2.jpg\" style=\"float: left; padding-right: 20px;\">En 2015, le Haut conseil à l’égalité entre les femmes et les hommes a révélé que <b>100% des femmes</b> ont été au moins une fois dans leur vie victime de <b>harcèlement</b> dans les transports en commun.<br>
 <br>
Plus que jamais d’actualité, le <b>harcèlement sexuel et sexiste</b> est au cœur de nos sociétés. Dans les rues, les transports, les espaces urbains, les femmes peinent à trouver leur place et à se sentir confiantes et en sécurité. En effet, les remarques et comportements  déplacés qu’elles sont susceptibles de <b>subir au quotidien</b> provoquent un sentiment de <b>mal-être et d’inconfort</b> pour les victimes, et contribuent aux <b>inégalités</b> encore prégnantes entre hommes et femmes.<br>
 <br>
Pour que les femmes <b>se réapproprient</b> enfin <b>l’espace public</b> et puissent se déplacer librement en ville, combattre le harcèlement est donc <b><u>un effort à fournir de la part de tous</u></b>.
Pour contribuer à cette lutte, nous avons mis en place HarLESSment.
</p></font>
						  </div>
						</div>
					 </div>
					 <hr>
					 <br>
					 <div class=\"row\">
						<div class=\"col s12 m12\">
						  <div class=\"icon-block\">
						  <div class=\"row center\">
						  <img class=\"responsive-img\" width=\"50%\" src=\"$url/web/img/logo.png\">
						  </div>
							<h5 class=\"center\">Qu’est-ce que HarLESSment ?</h5>
				
							<font size='4em'><p class=\"light center-align\" style=\"text-align: justify\">“HarLESSment” est la contraction de l’anglais “harassment” (“harcèlement”) et “less” (“moins”).
Comme son nom l’indique, HarLESSment a donc pour but de diminuer le harcèlement que subissent les femmes dans l’espace urbain, grâce à la prévention, l’entraide et la libération de la parole des victimes. Elles sont encore trop souvent blâmées pour ce qu’elles subissent.
 
Pour cela, HarLESSment vous propose ses services pour traiter le harcèlement en trois temps.</p></font>
						  </div>
						</div>
					 </div>

					 <div class=\"row\">

						<div class=\"col s12 m4\">
						<div class=\"card-panel lime darken-3\">
          <span class=\"white-text\">
						  <div class=\"icon-block\">
							<h5 class=\"center\">AVANT</h5>
							
							<font size='4em'><p class=\"light center-align\" style=\"text-align: justify\">- Tracer votre itinéraire et repérer en amont les lieux sûrs – mis en place avec la collaboration de votre ville - où vous pourrez trouver de l’aide en cas de besoin.
<br><br>- Trouver des solutions pour répondre au harcèlement et le combattre en tant que témoin</p></font>
						  </div>
						  </span></div>
						</div>
						<div class=\"col s12 m4\">
						<div class=\"card-panel red darken-4\">
          <span class=\"white-text\">
						  <div class=\"icon-block\">
							<h5 class=\"center\">PENDANT</h5>
				
							<font size='4em'><p class=\"light center-align\" style=\"text-align: justify\">- Alerter en un glissement de pouce toutes les personnes qui possèdent l’application et qui pourront alors vous porter secours grâce au système de géo localisation</p></font>
						  </div>
						  </span></div>
						</div>
						<div class=\"col s12 m4\">
						<div class=\"card-panel teal lighten-2\">
          <span class=\"white-text\">
						  <div class=\"icon-block\">
							<h5 class=\"center\">APRES</h5>
				
							<font size='4em'><p class=\"light center-align\" style=\"text-align: justify\">- Témoigner de votre expérience pour briser le silence et libérer la parole
<br><br>- Prendre contact avec d’autres victimes de votre ville pour vous créer un réseau de confiance et de soutien
<br><br>- Contribuer à un système d’information qui permettra à la ville de prendre des mesures adaptées
</p></font>
						  </div>
						  </span></div>
						</div>
					 </div>

					</div>
				  </div>
				
				  
				  </div>";

		return $html;
	}

	public function notFound() {
		echo "404 NOT FOUND";
	}

}