<?php
/**
 * Created by PhpStorm.
 * User: Bertrand
 * Date: 05/06/2016
 * Time: 11:44
 */

namespace app\view;


use app\model\Motcle;
use app\model\Temoignage;

class TemoignageView extends AbstractView {


    public function render($selector) {
        switch($selector) {
            case 1:
                echo $this->printFormTemoignage();
                break;
            case 2:
                echo $this->printTemoignages();
                break;
        }
    }

    public function printFormTemoignage() {

        $chips = '';

        foreach(Motcle::all() as $mc) {
            $chips .= '<div class="chip">' . $mc->chip . '<i class="material-icons">close</i></div>';
        }

        $html = '<br><br>
                <div id="wrapper">
                <div class="col s12 offset-s3" id="frame" role="content">
                    <h4>Présentez votre témoignage</h4>
                    <br>
                    <div id="divTemoignage" style="width: 80%; margin: auto;" class="col s8">
                        <form method="post" action="creerTemoignage">
                           <div class="input-field col s12">
                                <input type="text" id="lieu" class="col s9" name="lieu" required>
                                <label for="lieu">Lieu du harcèlement</label>
                           </div>
                           <br><br>
                           <div class="input-field col s12" style="width: 100%">
                                  <textarea id="text" class="materialize-textarea" name="texte" required></textarea>
                                  <label for="textarea1">Votre témoignage</label>
                                </div>    
                           <br><br>
                           <div class="input-field col s12">
                                <input type="text" id="tags" name="cles">
                               Mots clés à choisir : <br>' . $chips . '
                            </div>
                            <br><br>
                             <button id="send" class="btn waves-effect waves-light" type="submit" name="envoyer" value="envoi">Envoyer
                                <i class="material-icons right">send</i>
                              </button> 
                        </form>
                    </div>
                    
                    <br><br>
                </div>
            </div><br><br>';
            


        return $html;

    }

    public function printTemoignages() {

        $temoignages = Temoignage::all();
        $html = '<div>';

        foreach($temoignages as $t) {
            $html .= '<h1>Témoignage n°' . $t->id . '</h1>
                      <p>Lieu du harcèlement :' . $t->lieu . '</p>
                      <p>Témoignage : ' . $t->texte . '</p>
                      <p>Mot clés : ' . $t->motcles . '</p>
                      <hr>';
        }

        $html .= '</div>';

        echo $html;

    }
}