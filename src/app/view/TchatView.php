<?php

namespace app\view;

use app\model\Tchat;

class TchatView extends AbstractView
{

   

    public function __construct() {
    }

    public function render($selector)
    {
        switch ($selector) {
            case 1:
                echo $this->printTchat();
                break;
            default:
                echo $this->notFound();
                break;
        }
    }


    public function printTchat() {

        $html = '
            <br><br>    
            <div id="wrapper" class="wrapperTchat">
                <div class="col s12 offset-s3" style="width: 100%" id="frame" role="content">
                    <h4>Tchat d\'entraide</h4>
                    <div class="conversation" id="conversation">
                     <div class="status-bar">
                        <div class="nbConnectes"></div>
                        <div class="coStatus"></div>
                        <div class="fontawesome-bell"></div>
                        <div class="entypo-chat"></div>
                        <div class="entypo-battery"></div>
                        <div class="entypo-signal"></div>
                        <div class="fontawesome-signal"></div>
                        <div class="time"></div>
                        <div class="entypo-clock"></div>
                      </div>
                    </div>
                    <div id="divTchat" style="width: 100%" class="col s12">
                        <form>
                           <div class="row" style="width: 100%">
                                <div class="input-field col s12" style="width: 100%">
                                  <textarea id="text" class="materialize-textarea"></textarea>
                                  <label for="textarea1">Votre Message</label>
                                </div>
                              </div>
                            <br><br>
                            <div class="row center">
                              <button id="send" class="btn waves-effect waves-light" type="submit" name="action">Envoyer
                                <i class="material-icons right">send</i>
                              </button>
                            </div>  
                        </form>
                    </div>
                    
                    <br><br>
                </div>
            </div>';
        return $html;
    }
}