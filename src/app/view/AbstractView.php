<?php

namespace app\view;

abstract class AbstractView {

    public function renderHeader($content = null) {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);


        $html = <<<END
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="$url/web/images/favicon.ico" type="images/x-icon">
    <title>HarLESSment</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="$url/web/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="$url/web/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="$url/web/css/fonts.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <style media="screen " type="text/css"> 
    .hoverNav:hover {
    background-color: rgba(255,255,255,0.1) !important;
    }
    #nav {
    text-decoration: underline !important;
    }
    @media only screen and (max-width: 665px) {
      #nav > li {
      display: block !important;
    }
      #nav > li > a {
      float: none !important;
    }
    #gotop {
    display: none !important;
  }
    }
    </style>
$content   
</head>
<body style="background-image: url($url/web/img/motif.png)">
  <div class="fixed-action-btn" id="gotop" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large transparent" href="#above">
      <i class="large material-icons brown">navigation</i>
    </a>
  </div>
<ul id="dropavant" class="dropdown-content  brown darken-3">
  <li class="hoverNav"><a href="#!" class="white-text">Votre itinéraire sécurisé</a></li>
  <li class="divider"></li>
  <li class="hoverNav"><a href="$url/quizz" class="white-text">Testez-vous sur un quizz</a></li>
  <li class="divider"></li>
  <li class="hoverNav"><a href="$url/harcelement" class="white-text">Qu'est ce que le harcèlement ?</a></li>
  <li class="hoverNav"><a href="$url/temoin" class="white-text">Témoin : Comment agir ?</a></li>
  <li class="hoverNav"><a href="$url/harcelee" class="white-text">Harcelée : Comment réagir ?</a></li>
</ul>
<ul id="dropapres" class="dropdown-content  brown darken-3">
    <li class="hoverNav"><a href="$url/tchat" class="white-text">Tchat d'entraide</a></li>
  <li class="hoverNav"><a href="$url/creerTemoignage" class="white-text">Présenter un témoignage</a></li> 
  <li class="hoverNav"><a href="$url/temoignages" class="white-text">Voir les témoignages</a></li> 
</ul>
<a href="$url/"><img class="responsive-img" src="$url/web/img/banniere.png"></a>
<ul id="nav" style="text-align: center; background-color: rgba(0,0,0,0.1); padding: 10px 0; margin-top: -6px;">
           <li style="display: inline;"><a class="dropdown-button brand-logo lime-text darken-4 bold left" href="#!" style="font-family:Tahoma; width: 30%;" data-activates="dropavant"><img class="responsive-img" src="$url/web/img/avant.png"></a></li>
            <li style="display: inline;"><a class="brand-logo deep-orange-text darken-4 bold center" href="#!" style="font-family:Tahoma; width: 30%; text-align: center;" ><img class="responsive-img" src="$url/web/img/pendant.png"></a></li>
            <li style="display: inline;" ><a class="dropdown-button brand-logo indigo-text bold right" href="#!" style="font-family:Tahoma; width: 30%; text-align: center;" data-activates="dropapres"><img class="responsive-img" src="$url/web/img/apres.png"></a></ul>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
END;

      $htmlResponsive = <<<END
      <ul class="side-nav" id="mobile-demo">
END;

      $html .= '</ul>';
      $htmlResponsive .= '</ul>';
      $html .= $htmlResponsive . '</div></nav>';

      echo $html;

    }


    public function renderFooter($content = null) {
      $app = \Slim\Slim::getInstance();
$url = $app->request->getRootUri();
$url = str_replace("/index.php", "", $url);
        $footer = <<<END
                   <footer class="page-footer brown darken-1">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Nos partenaires :</h5>
                <p class="grey-text text-lighten-4">Logo 1 - Logo 2 - Logo 3</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Sites utiles :</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="http://www.madmoizelle.com/">madmoizelle.com</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://www.stopharcelementderue.org/">Association Stopharcelementderue</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://projetcrocodiles.tumblr.com/">Le projet crocodile</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://payetashnek.tumblr.com/">Paye ta schnek</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2016 Copyright Hackathon
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
        <div id="output" align="center"></div>
<script type="text/javascript" src="$url/web/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="$url/web/js/materialize.min.js"></script>
<script type="text/javascript" src="$url/web/js/init.js"></script>
$content
<script type="text/javascript">
    $(document).ready(
        function(){
            $('.btn-floating').click(function(){
                var offset = $('#nav').offset().top
                $('html,body').animate(
                    {scrollTop: offset},'slow');
            });
        });
</script>
</body>
</html>
END;
        echo $footer;
    }

    public abstract function render($selector);

}