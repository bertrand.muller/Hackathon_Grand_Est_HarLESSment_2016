<?php

namespace app\view;

use app\model\Question;

class QuizzView extends AbstractView {

    private $message;
    private $parties;

    public function __construct($m,$part) {
        $this->message = $m;
        $this->parties = $part;
    }

    public function render($selector)
    {
        switch ($selector) {
            case 1:
                echo $this->printQuiz();
                break;
            case 2:
                echo $this->printHarcelement();
                break;
            case 3:
                echo $this->printTemoin();
                break;
            case 4:
                echo $this->printHarcelee();
                break;
            case 5:
                echo $this->printBouton();
                break;
        }
    }


    public function printQuiz() {

        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);
        $questions = Question::all();

        $html = '<br><br><div class="col s6 offset-s3" id ="frame" role="content"></div>
                <script>
                    var quest = [];
                    var i = 0;';

                    foreach($questions as $q) {
                        $html .= 'quest[i] = new Array("' . $q->reponse1 . '","' . $q->reponse2 . '","' . $q->reponse3 . '","' . $q->reponse4 . '");';
                        $html .= 'i = i + 1;';
                    }

        $html .= 'window.questions = quest;
                  window.url = "' . $url . '"
                </script>';
        return $html;
    }

    public function printHarcelement() {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $html = <<<END
        <div class=\"container\">
                    <div class=\"section\">
        <div class="row">
                        <div class="col s12 m12">
                          <div class="icon-block">
                          <br>
                            <h5 class="center"><u>Qu’est-ce que le harcèlement ?</u></h5>
                
                            <font size='4em'><p class="light center-align" style="text-align: justify">L’association <a class="blue-text text-darken-4" href="http://www.stopharcelementderue.org/?page_id=717">#Stopharcelementderue</a> définie le harcèlement ainsi :<br>     
</p></font><div class="card-panel white lighten-2" style="opacity: 0.8;">« Le harcèlement de rue, ce sont les comportements adressés aux personnes dans les espaces publics et semi-publics, visant à les interpeler verbalement ou non, leur envoyant des messages intimidants, insistants, irrespectueux, humiliants, menaçants, insultants en raison de leur sexe, de leur genre ou de leur orientation sexuelle.<br>
Vous savez, les sifflements, les commentaires sexistes, les interpellations ou insultes, voire les attouchements… Ces comportements touchent les femmes et les personnes LGBT dans la rue, les bars, les transports et les espaces publics.<br>
Leurs répétitions ou leur violence génèrent un environnement hostile à ces personnes et portent une atteinte inacceptable à leur dignité et à leur liberté. Ce n’est pas de l’humour, ce ne sont pas des compliments, et ce n’est certainement pas de la drague ! Nombre de femmes apprennent à baisser la tête, ne pas répondre, changer de trottoir ou s’habiller différemment. Bref, elles se sentent moins en sécurité, moins autonomes. Par crainte, elles deviennent moins ouvertes aux vraies rencontres, moins enclines à aller draguer ou à se laisser draguer. Et c’est bien dommage. »</div><br>

En effet, le harcèlement sexuel et sexiste est aujourd’hui un combat d’actualité pour nombre d’associations et pour le gouvernement. Il revêt des formes très diverses (regards appuyés, remarques, insultes, menaces, sifflements, gestes déplacés, empiétement de l’espace intime, exhibition sexuelle, attouchements, …) qui sont pourtant passibles des peines suivantes : 

</p></font><br><br> <div class="row center"><img class="responsive-img" src="$url/web/img/harc1.jpg"></div>

Mais pour saisir l’ampleur du harcèlement sexuel et le sentiment de malaise qu’il entraîne, le plus parlant est parfois de se plonger au cœur des témoignages.<br> 

<br> <div class="row center"><img class="responsive-img" width="70%" src="$url/web/img/harc2.jpg"></div>

Petite liste non exhaustive de sites de témoignages : 
<ul><li>- Le Projet crocodile (BD qui s’appuie sur des témoignages pour dénoncer le harcèlement) : <a href="http://projetcrocodiles.tumblr.com/" class="blue-text text-darken-4">http://projetcrocodiles.tumblr.com/</a> </i> 
<li>- Le tumblr Harcelement de rue (témoignages photos) : <a href="http://harcelementderue.tumblr.com/" class="blue-text text-darken-4">http://harcelementderue.tumblr.com/</a></li>
<li>- Le tumblr Payes ta schnek (qui recense les phrases entendus par les harceleurs) : <a href="http://payetashnek.tumblr.com/" class="blue-text text-darken-4">http://payetashnek.tumblr.com/</a></li><br>
Et bien sûr l’onglet « Témoignage » d’HarLESSment qui vous permettra de voir des témoignages associés à la localisation des lieux du délit, et de poster des commentaires de soutiens envers la victime.
</ul></p></font>
                          </div>
                        </div>
                     </div>
                     </div>
                     </div>
END;
echo $html;
    }

    public function printTemoin() {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $html = <<<END
        <div class=\"container\">
                    <div class=\"section\">
        <div class="row">
                        <div class="col s12 m12">
                          <div class="icon-block">
                          <br>
                            <h5 class="center"><u>Comment réagir en tant que témoin ?</u></h5><br>

                            <div class="row center"><img class="responsive-img" src="$url/web/img/temoin.png"></div>  
                
                            <font size='4em'><p class="light center-align" style="text-align: justify">La position de témoin n’est pas une position facile. On peut craindre d’être mis en danger, d’être harcelé à son tour, ou même de se méprendre sur la situation. Mais l’action en tant que témoin est plus que nécessaire : elle est un devoir moral. C’est en brisant l’individualisme qui nous isole au sein de nos sociétés que nous permettrons à chacun de circuler librement et confortablement dans l’espace public.<br> 
Néanmoins, le témoin doit toujours veiller à ne pas se mettre en danger lui non plus et doit donc procéder à une analyse en amont afin de déterminer ce qu’il convient le mieux de faire. Le projet crocodile dédit là encore plusieurs planche détaillées qui proposent plusieurs réactions possibles de la part des témoins :<br> 
<div class="center"><a class="blue-text text-darken-4" href="http://projetcrocodiles.tumblr.com/post/86299616713/pour-plus-de-clart%C3%A9-jai-chang%C3%A9-un-peu-de">Projet Crocodile (Témoins)</a></div>
</p></font><br><div class="row center"><img class="responsive-img" src="$url/web/img/croco1.png"></div>
<font size='4em'><p class="light center-align" style="text-align: justify">L’entraide et la bienveillance doivent retrouver une place au cœur de notre société, et nous comptons sur cette bienveillance mutuelle des citadins pour que se développe le réseau d’HarLESSment, qui permettra désormais à tous les utilisateurs de l’application de recevoir une notification à chaque alerte harcèlement envoyée par les victimes pour agir aux points à proximité.<br>
Nous comptons donc sur la collaboration de tous les utilisateurs pour réduire le harcèlement et surtout le sentiment d’insécurité grâce à la solidarité, la surveillance et l’action active de chacun.<br>
<br><div class="row center"><img class="responsive-img" src="$url/web/img/croco2.jpg"></div>
</p></font>
                          </div>
                        </div>
                     </div>
                     </div>
                     </div>
END;
echo $html;
    }

     public function printHarcelee() {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $html = <<<END
        <div class=\"container\">
                    <div class=\"section\">
        <div class="row">
                        <div class="col s12 m12">
                          <div class="icon-block">
                          <br>
                            <h5 class="center"><u>Comment réagir en tant qu’harcelée ?</u></h5><br>

                            <div class="row center"><img class="responsive-img" src="$url/web/img/stop.jpg"></div>  
                
                            <font size='4em'><p class="light center-align" style="text-align: center">En tant qu’harcelée, réagir n’est souvent pas facile, surtout en tant que personne isolée.<br>
Le projet crocodile compte à son actif plusieurs planches très détaillées sur certaines options qui peuvent s’offrir à vous et que vous pouvez consulter ici :<br>
</p></font><div class="center"><a class="blue-text text-darken-4" href="http://projetcrocodiles.tumblr.com/post/78830110539/le-site-hollaback-clic-avec-sa-page-comment">Projet Crocodile (Harcelée)</a></div><br>

<font size='4em'><p class="light center-align" style="text-align: center">Il convient bien d’analyser le profil du ou des agresseurs et d’aviser alors d’une solution qui ne vous mette pas en danger.<br>

De plus, sortir de l’isolement et trouver des alliés peut également permettre de mettre en fuir le ou les harceleurs et de se sentir davantage en confiance. Mais les réactions et l’aide des témoins se fait parfois attendre.<br><br>
</p></font>Comme le souligne le site <a class="blue-text text-darken-4" href="http://www.madmoizelle.com/reagir-temoins-projet-crocodile-255842">madmoizelle.com</a> : <br>
<div class="card-panel white lighten-2" style="opacity: 0.8;">« L’absence de réaction est souvent due <b>au phénomène de dilution de la responsabilité</b>, ou « effet témoin », que Justine nous avait bien expliqué.</br>
<br>Pour casser « l’effet témoin », le conseil aux victimes d’agression est simple. <b>Il ne faut pas appeler  « à l’aide » à la cantonade, mais plutôt interpeller une personne en particulier</b> : monsieur avec la chemise à carreaux, madame avec le chignon, etc. La personne se sent concernée, elle ne se pose plus la question de savoir si elle doit intervenir ou pas (vous venez de résoudre ce dilemme pour elle). »<br>
</div>

<font size='4em'><p class="light center-align" style="text-align: justify">Retisser le lien social, favoriser l’entraide et permettre aux victimes de bénéficier d’aide lors de ces moments difficiles à gérer seul est aussi l’un des enjeux de HarLESSment. En téléchargeant l’appli sur votre mobile, vous pourrez lancer à tout instant une alerte qui émettra une notification sur le portable de ceux qui possèdent également l’application, et qui pourront alors vous aider s’ils se trouvent à proximité grâce au système de géolocalisation.<br>
*placer un lien de téléchargement*

On ne le répétera jamais assez mais vous n’êtes pas responsable du harcèlement que vous subissez. Ni tenue, ni attitude, ni heure tardive ne justifient que l’on vous harcèle. Il en va de la liberté que chacune puisse circuler au même titre que chacun dans l’espace urbain sans subir de remarques, qui ne sont que la conséquence d’une société malade empreinte de culture du viol, amenant les femmes à être sans cesse réifiées et ramenée à leur physique.</p></font>
                          </p></font>
                          <div class="row center"><img class="responsive-img" src="$url/web/img/harc3.jpg"></div>

<font size='4em'><p class="light center-align" style="text-align: center">(Mieux comprendre la culture du viol sur <a class="blue-text text-darken-4" href="http://www.madmoizelle.com/je-veux-comprendre-culture-du-viol-123377">cet article</a> de madmoizelle.com)<br><br>
Si vous êtes victime de harcèlement, briser le silence peut également vous aider et vous pourrez trouver de l’aide et du soutien en témoignant directement sur l’application HarLESSment ou sur d’autres sites prévus à cet effet.</p></font>

<div class="row center"><img class="responsive-img" src="$url/web/img/harc4.jpg"></div>  

                          </div>
                        </div>
                     </div>
                     </div>
                     </div>
END;
echo $html;
    }

    public function printBouton() {
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $url = str_replace("/index.php", "", $url);

        $html = <<<END
        <div class=\"container\">
                    <div class=\"section\">
        <div class="row">
        <label class="switch">
  <input type="checkbox">
  <div class="slider round"></div>
</label>
                     </div>
                     </div>
                     </div>
END;
echo $html;
    }
}