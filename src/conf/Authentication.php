<?php
namespace conf;
use app\model as Model;
use app\controller as Controlleur;

class Authentication {
	

	public function __construct() {

	}

	public static function authenticate($email, $pass) {
		$user = Model\User::where('email',$email)->first();

		if (isset($user)) {

			if (!password_verify($pass, $user->password)) {
				throw new AuthException("Le mot de passe saisi est incorrect.", 1);
			}
		} else {
			throw new AuthException("Il n'existe aucun compte lié à l'addresse e-mail " . $email . " saisie.", 1);
		}

	}

	public static function loadProfil($email) {
		$user = Model\User::where('email',$email)->first();

		if (isset($user)) {
			session_destroy();
			session_start();
			$_SESSION['connected'] = true; //connected
			$_SESSION['email'] = $email;
			$_SESSION['auth_level'] = $user->privilege;
			$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION['avatar']= $user->avatar;
			$_SESSION['name'] = $user->name;
			$_SESSION['surname'] = $user->surname;
			$_SESSION['accepted'] = $user->accepted;
			$_SESSION['dernierActivite'] = time();
			$_SESSION['dateCreationSession'] = time();
			$_SESSION['accepted'] = $user->accepted;

		} else {
			throw new AuthException("Erreur email incorrect", 1);
			
		}
	}

	/**
	 * Fonction qui permet de vérifier que les droits d'accès de l'utilisateur correspondent aux droits d'accès de la fonctionnalité
	 */
	public static function checkAccessRights($required_level = 0) {
		if (isset($_SESSION['connected'])) {
			if ($required_level != $_SESSION['auth_level'] || $_SESSION['accepted'] == 0) {
				throw new AuthException("Accès refusé : authentification level trop bas, ou inscription non encore acceptée", 1);

			}
		}
	}

	/**
	 * Fonction qui permet de vérifier l'état de la session
	 * vérification de l'adresse IP, vérification du temps de non activité (temps > 30 min entraine une déconnexion)
	 * regénération de l'id de session au bout de 10 min
	 * @throws AuthException exception lancée si la vérification a échoué avec un message personnalisé
	 */
	public static function checkSession() {
		$app = \Slim\Slim::getInstance();
	    $url = $app->request->getRootUri();
	    $url = str_replace("/index.php", "", $url);
		
		if ($_SESSION['connected']) { // verifier connexion
			if ($_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) { // verifier IP
				if (isset($_SESSION['dernierActivite']) && (time() - $_SESSION['dernierActivite'] > 36000)) { // verifier time stamp
					$message = <<<END
			<h5 class="light">TIME OUT. Vous n'avez pas eu d'activité depuis plus de 60 minutes. Veuillez vous reconnecter en cliquant <a class="teal-text" href="$url/index.php/connexion">ICI</a>.</h5>
END;
					$control = new Controlleur\UserController();
					$control->logOut();
					throw new AuthException($message, 1);
				} else { // cas où tout se passe bien
					$_SESSION['dernierActivite'] = time();
				
					if (time() - $_SESSION['dateCreationSession'] > 600) {
					    // session débutée il y a plus de 10 minutes
					    session_regenerate_id(true);
					    $_SESSION['dateCreationSession'] = time();
					}

				}
			} else {
				$message = <<<END
			<h5 class="light">Vous avez changé d'adresse IP. Veuillez vous reconnecter en cliquant <a class="teal-text" href="$url/index.php/index.php/connexion">ICI</a>.</h5>
END;
				$control = new Controlleur\UserController();
				$control->logOut();
				throw new AuthException($message, 1);
			}
		} else {
			$message = <<<END
			<h5 class="light">Vous n'êtes pas connecté. Veuillez vous connecter en cliquant <a class="teal-text" href="$url/index.php/connexion">ICI</a>.</h5>
END;
			$control = new Controlleur\UserController();
			$control->logOut();
			throw new AuthException($message, 1);
		}
			
	}

	public static function createUser($email, $userRights, $mdp, $nom, $pren) {
		$hashMdp = password_hash($mdp, PASSWORD_DEFAULT);

		$user = new Model\User();
		$user->email = $email;
		$user->privilege = $userRights;
		$user->password = $hashMdp;
		$user->surname = $pren;
		$user->name = $nom;
		try {
			$user->save();
		} catch(\PDOException $e) {
			throw new AuthException("L'inscription n'a pas pu avoir lieu", 1);
		}	 

	}
}