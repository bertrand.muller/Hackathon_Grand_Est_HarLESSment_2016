<?php

require_once "vendor/autoload.php";

session_start();
if (!isset($_SESSION['connected']))
    $_SESSION['connected'] = false;

use app\model;
use app\controller as Controller;
use app\model\Question;
use conf\ConnectionFactory as ConnectionFactory;

ConnectionFactory::setConfig('db.conf.ini');

$db = ConnectionFactory::makeConnection();

if(isset($_POST['question']) && isset($_POST['reponse'])) {

    $numRep = intval($_POST['reponse']);
    $numQuest = intval($_POST['question'])-1;
    $quest = Question::find($numQuest);

    switch($numRep) {
        case "0":
            $quest->reponse1 = Question::find($numQuest)->reponse1 + 1;
            break;
        case "1":
            $quest->reponse2 = Question::find($numQuest)->reponse2 + 1;
            break;
        case "2":
            $quest->reponse3 = Question::find($numQuest)->reponse3 + 1;
            break;
        case "3":
            $quest->reponse4 = Question::find($numQuest)->reponse4 + 1;
            break;
    }

    $quest->save();

}