-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 05 Juin 2016 à 16:38
-- Version du serveur :  10.0.17-MariaDB
-- Version de PHP :  5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `grandest`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id1` int(15) NOT NULL,
  `id2` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `motcle`
--

CREATE TABLE `motcle` (
  `chip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `motcle`
--

INSERT INTO `motcle` (`chip`) VALUES
  ('Attouchements'),
  ('Bus'),
  ('Impuissance'),
  ('Insultes'),
  ('Intervention de témoins'),
  ('Perte de repères'),
  ('Public'),
  ('Rapide'),
  ('Train'),
  ('Violence');

-- --------------------------------------------------------

--
-- Structure de la table `pointsur`
--

CREATE TABLE `pointsur` (
  `id` int(15) NOT NULL,
  `description` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `id` int(15) NOT NULL,
  `reponse1` int(15) NOT NULL DEFAULT '0',
  `reponse2` int(15) NOT NULL DEFAULT '0',
  `reponse3` int(15) NOT NULL DEFAULT '0',
  `reponse4` int(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `question`
--

INSERT INTO `question` (`id`, `reponse1`, `reponse2`, `reponse3`, `reponse4`) VALUES
  (0, 6, 5, 8, 10),
  (1, 3, 4, 10, 1),
  (2, 2, 3, 11, 0),
  (3, 2, 2, 9, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tchat`
--

CREATE TABLE `tchat` (
  `idDest` int(15) NOT NULL,
  `idExpe` int(15) NOT NULL,
  `heure` int(15) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `temoignage`
--

CREATE TABLE `temoignage` (
  `id` int(15) NOT NULL,
  `lieu` varchar(250) NOT NULL,
  `texte` text NOT NULL,
  `motcles` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `temoignage`
--

INSERT INTO `temoignage` (`id`, `lieu`, `texte`, `motcles`) VALUES
  (1, 'Nancy', 'Agression dans la rue', 'Attouchements,Bus,Insultes,Perte de repères'),
  (2, 'Nancy', 'Mauvaise rencontre', 'Attouchements,Bus,Insultes,Perte de repères'),
  (3, 'Paris', 'Rue sombre', 'Attouchements');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(15) NOT NULL,
  `sexe` int(1) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `adresse` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id1`,`id2`);

--
-- Index pour la table `motcle`
--
ALTER TABLE `motcle`
  ADD PRIMARY KEY (`chip`);

--
-- Index pour la table `pointsur`
--
ALTER TABLE `pointsur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `temoignage`
--
ALTER TABLE `temoignage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `pointsur`
--
ALTER TABLE `pointsur`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `temoignage`
--
ALTER TABLE `temoignage`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
