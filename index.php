<?php

require_once "vendor/autoload.php";

session_start();
if (!isset($_SESSION['connected']))
    $_SESSION['connected'] = false;

use app\model;
use app\controller as Controller;
use conf\ConnectionFactory as ConnectionFactory;
use \Slim\Slim;

ConnectionFactory::setConfig('db.conf.ini');

$db = ConnectionFactory::makeConnection();

$quizzController = new Controller\QuizzController();
$adminController = new Controller\AdminController();
$tchatController = new Controller\TchatController();
$temoignageController = new Controller\TemoignageController();

$app = new slim();

$app->get('/', function () {
    $control = new Controller\MainController();
    $control->getHome();
});

$app->get('/home', function () {
    $control = new Controller\MainController();
    $control->getHome();
});

$app->get('/quizz', function() use ($quizzController) {
    $quizzController->getQuiz();
});

$app->get('/harcelement', function() use ($quizzController) {
    $quizzController->getHarcelement();
});

$app->get('/temoin', function() use ($quizzController) {
    $quizzController->getTemoin();
});

$app->get('/harcelee', function() use ($quizzController) {
    $quizzController->getHarcelee();
});

$app->get('/pendant', function() use ($quizzController) {
    $quizzController->getPendant();
});

$app->get('/tchat', function() use ($tchatController) {
    $tchatController->getTchat();
});

$app->get('/creerTemoignage', function() use ($temoignageController) {
    $temoignageController->getFormTemoignage();
});

$app->post('/creerTemoignage', function() use ($temoignageController,$app) {
   $temoignageController->saveTemoignage($app->request->post());
});

$app->get('/temoignages', function() use ($temoignageController) {
    $temoignageController->getTemoignages();
});

$app->notFound(function () {
    $control = new Controller\MainController();
    $control->getNotFound();
});

$app->run();
