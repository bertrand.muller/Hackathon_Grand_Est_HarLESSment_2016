(function($){
	$(function(){

		$('.button-collapse').sideNav();
		$('.parallax').parallax();


		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15 // Creates a dropdown of 15 years to control year
		});
		
		
		
		
//		var $btnnewstep		= $('#btn_new_step');
//		var $hstepsnbr		= $('#hstepsnbr');
//		var $partyconfig	= $('#party-configuration');
//		var t = ''+
//			'<div class="row">'+
//				'<a class="btn-floating btn-small waves-effect waves-light red" style="bottom:8px; right:-34px;"><i class="material-icons">remove</i></a>'+
//				'<p class="col s2 m2 l2 offset-s2 offset-m2 offset-l2">'+
//					'<div class="col s1 m1 l1">'+
//						'<input type="checkbox" class="filled-in" id="capero" />'+
//						'<label for="capero">Apéro</label>'+
//					'</div>'+
//					'<div class="col s1 m1 l1">'+
//						'<input type="checkbox" class="filled-in" id="cresto" />'+
//						'<label for="cresto">Resto</label>'+
//					'</div>'+
//					'<div class="col s1 m1 l1">'+
//						'<input type="checkbox" class="filled-in" id="cafter" />'+
//						'<label for="cafter">After</label>'+
//					'</div>'+
//				'</div>'+
//			'</div>';
//		
//		$btnnewstep.click(function() {
//			try {
//				var nbr = parseInt($hstepsnbr.val());
//				if (nbr < 4) {
//					$partyconfig.append(t);
//					$hstepsnbr.val(++nbr);
//				}
//			} catch (e) {}
//		});

	}); // end of document ready
})(jQuery); // end of jQuery name space

function addConf() {
	var i = $('#aConf').attr('data-val');
	i++;
	if (i <= 4) {
		var title = "Deuxième";
		if (i == 3)
			title = "Troisième";
		else if (i == 4)
			title = "Quatrième";
		

		var content = '<div id="party-configuration_'+i+'" class="row">';
				content += '<h3 class="center-align">'+title+' configuration</h3>';
				content += '<h5 class="center-align">Première étape</h5>';
				 content += '<div class="input-field col s12">';
					content += '<select name="type_etape_1" class="browser-default" required>';
						content += '<option value="apero" selected>Apéro</option>';
						content += '<option value="restaurant">Restaurant</option>';
						content += '<option value="after">After</option>';
					content +='</select>';
				content += '</div>';
				content += '<div class="col s12"><div class="col s3"><h5>Heure de début</h5></div>';
					content += '<div class="col s3">';
					content +='<input id="heure_deb_etap_1" name="heure_deb_etap_1" type="number" min="0" max="24" required></div><div class="col s1"><h5>:</h5></div>';
					content += '<div class="col s3"><input id="min_deb_etap_1" name="min_deb_etap_1" type="number" min="0" max="60" required></div></div><div class="col s12"><div class="col s3">';
						content += '<h5>Heure de fin</h5></div><div class="col s3"><input id="heure_fin_etap_1" name="heure_fin_etap_1" type="number" min="0" max="24" required></div>';
					content += '<div class="col s1"><h5>:</h5></div><div class="col s3"><input id="min_fin_etap_1" name="min_fin_etap_1" type="number" min="0" max="60" required></div></div>';
				content += '<h5 class="center-align">Deuxième étape</h5>';
				 content += '<div class="input-field col s12">';
					content += '<select name="type_etape_1" class="browser-default"><option value="apero">Apéro</option><option value="restaurant" selected>Restaurant</option><option value="after">After</option></select>';
				content += '</div><div class="col s12"><div class="col s3"><h5>Heure de début</h5></div><div class="col s3"><input id="heure_deb_etap_2" name="heure_deb_etap_2" type="number" min="0" max="24">';
					content += '</div><div class="col s1"><h5>:</h5></div><div class="col s3"><input id="min_deb_etap_2" name="min_deb_etap_2" type="number" min="0" max="60"></div>';
				content += '</div><div class="col s12"><div class="col s3"><h5>Heure de fin</h5></div><div class="col s3"><input id="heure_fin_etap_2" name="heure_fin_etap_2" type="number" min="0" max="24">';
					content += '</div><div class="col s1"><h5>:</h5></div><div class="col s3"><input id="min_fin_etap_2" name="min_fin_etap_2" type="number" min="0" max="60"></div></div>';
				content += '<h5 class="center-align">Troisième étape</h5><div class="input-field col s12"><select name="type_etape_1" class="browser-default"><option value="apero">Apéro</option><option value="restaurant">Restaurant</option><option value="after" selected>After</option></select>';
				content += '</div><div class="col s12"><div class="col s3"><h5>Heure de début</h5></div><div class="col s3"><input id="heure_deb_etap_3" name="heure_deb_etap_3" type="number" min="0" max="24"></div><div class="col s1"><h5>:</h5></div><div class="col s3">';
					content +=	'<input id="min_deb_etap_3" name="min_deb_etap_3" type="number" min="0" max="60"></div></div><div class="col s12"><div class="col s3"><h5>Heure de fin</h5></div><div class="col s3"><input id="heure_fin_etap_3" name="heure_fin_etap_3" type="number" min="0" max="24">';
					content += '</div><div class="col s1"><h5>:</h5></div><div class="col s3"><input id="min_fin_etap_3" name="min_fin_etap_3" type="number" min="0" max="60"></div></div></div>';
		$('#config').append(content);
		$('#aConf').attr('data-val', i);
	}
}