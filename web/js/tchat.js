$(document).ready(function() {
	var $time =  $('.time');
	var date_heure = function() {
		var date = new Date();
		var hour = date.getHours();
		var mins = date.getMinutes();
		hour = (hour > 9) ? hour : '0'+hour;
		mins = (mins > 9) ? mins : '0'+mins;
		var timeStr = hour + ':' + mins;
		if ($time.text() != timeStr) {
			$time.text(timeStr);
		}
	};
	date_heure();

	window.onload = function() {
		setInterval(date_heure, 5000); //Actualisation de l'heure
	};

	var chat = io('http://192.168.58.1:3000/chat');

	chat.on('connect', function() {
		$('.coStatus').html('<span style="color:green;">Connecté</span>');
	});
	chat.on('connect_error', function() {
		$('.coStatus').html('<span style="color:red;">Erreur de connexion</span>');
	});
	chat.on('disconnect', function() {
		$('.coStatus').html('<span style="color:red;">Erreur de connexion</span>');
	});
	chat.on('connection', function() {
		console.log('A user has connected');
	});
	chat.on('disconnection', function() {
		console.log('A user has disconnected');
	});
	chat.on('users_count', function(data) {
		var pers = ' personne';
		$('.nbConnectes').text(data.count+(data.count > 1 ? pers+'s' : pers));
	});

	var $text = $('#text');
	$('#send').on('click', function(e) {
		e.preventDefault();
		if ($text.val().length > 0) {
			chat.emit('message', {text: $text.val()});

			var elements = document.querySelectorAll('.krm, .luky');
			var currentDate = new Date().toLocaleTimeString();

			var texte = '<div class="luky">' +
				'<div class="content">' + $text.val() + '</div>' +
				'<div class="message-time">' + currentDate + '</div>' +
				'</div>';

			elements = document.querySelectorAll('.krm, .luky');
			var conversation = $('#conversation');
			conversation.append(texte);
			if(elements.length > 7) { conversation.scrollTo(elements[elements.length-1]); }
			$text.val('');
		}
		return false;
	});

	chat.on('message', function(data) {
		var elements = document.querySelectorAll('.krm, .luky');
		var currentDate = new Date(data.time*1000).toLocaleTimeString();
		var texte = '<div class="krm">' +
			'<div class="content">' + data.text + '</div>' +
			'<div class="message-time">' + currentDate + '</div>' +
			'</div>';
		var conversation = $('#conversation');
		conversation.append(texte);
		if(elements.length > 7) { conversation.scrollTo(elements[elements.length-1]); }
	});
});