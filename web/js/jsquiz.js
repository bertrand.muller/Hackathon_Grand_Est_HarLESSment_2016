var quiztitle = "Quizz sur le harcèlement sexuel";
var totalPourcentages = 0;

var quiz = [
    {
        "question" : "N°1 : Vous êtes dans la rue et vous assistez à une scène de harcèlement : une jeune femme se fait aborder par un homme qui vous parait insistant, elle semble être mal à l’aise et se trouver dans une position de refus. Vous : ",
        "image" : window.url + "/web/img/quiz/q1.jpg",
        "choices" : [
            "Intervenez directement",
            "Observez la scène pour contrôler l'évolution de la situation mais vous n'intervenez pas",
            "Intervenez qu'en cas d'agression physique",
            "Passez votre chemin"
        ],
        "correct" : "Intervenez directement",
        "explanation" : "Bien souvent, dans une situation de harcèlement, les témoins extérieurs n’interviennent pas, ce qui est dommageable pour la victime qui se sent isolée et vulnérable. L’impuissance qu’elle ressent peut à terme créer un sentiment d’insécurité généralisée, puisqu’elle se sent à la merci des potentiels harceleurs, ayant l’impression de n’être jamais secourue. " +
        "Si vous êtes témoin, vous pouvez agir tout en vous préservant. Rendez vous dans la rubrique « Comment réagir en tant que témoin ? »"
    },
    {
        "question" : "N°2 : Vous voyez une jeune femme vêtue d’une robe qui la met en valeur, et un homme la siffle et lui fait un compliment. Vous trouvez ce comportement :",
        "image" : window.url + "/web/img/quiz/q2.jpg",
        "choices" : [
            "Justifiable, vous auriez pu faire la même chose",
            "Innocent, vous ne le feriez pas mais vous ne voyez pas vraiment le mal",
            "Révoltant, ce n’est pas un comportement à adopter envers quelqu’un"
        ],
        "correct" : "Révoltant, ce n’est pas un comportement à adopter envers quelqu’un",
        "explanation" : "Ce comportement est condamnable et à bannir. Siffler ou klaxonner une personne n’est pas un comportement décent envers une personne, car on la réduit alors à l’état d’\"objet\". Une personne qui se déplace dans la rue n’attend pas que l’on valide ou invalide son apparence physique mais aspire simplement à se rendre d’un point A à un point B dans la tranquillité."
    },
    {
        "question" : "N°3 : Le harcèlement sexuel et sexiste dans l’espace public c’est :",
        "image" : window.url + "/web/img/quiz/q3.jpg",
        "choices" : [
            "Un mythe",
            "Anecdotique, ça arrive de temps à autre mais il faut le combattre",
            "Extrêmement répandu et il est temps de le gérer activement"
        ],
        "correct" : "Extrêmement répandu et il est temps de le gérer activement",
        "explanation" : "Selon une étude de 2015 de l’HCEfh, 100% des femmes ont été au moins une fois dans leur vie victime de harcèlement dans les transports en commun, même si elles n’avaient pas forcément conscience de l’anormalité de ce comportement elle-même. En effet, ce phénomène  est si répandu que les femmes l’intègrent progressivement et modifient leur comportement consciemment ou non pour l’éviter. " +
        "Il n’est plus aux victimes de changer leur attitude pour éviter les agressions, mais aux harceleurs d’arrêter d’agresser. Aucune tenue quelle qu’elle soit ne peut justifier le harcèlement."
    },
    {
        "question" : "N°4 : Le harcèlement, au final, c’est :",
        "image" : window.url + "/web/img/quiz/q4.png",
        "choices" : [
            "Sans gravité",
            "Certes trop répandu et gênant mais ça ne devrait pas être un combat prioritaire",
            "Impactant pour les victimes et pour l’harmonie sociale en générale donc il est important d’imaginer des solutions dès maintenant"
        ],
        "correct" : "Impactant pour les victimes et pour l’harmonie sociale en générale donc il est important d’imaginer des solutions dès maintenant",
        "explanation" : "Le harcèlement peut marquer durablement les victimes de par sa régularité et la menace quotidienne potentielle qu’il peut faire peser sur elle. De plus, le harcèlement ne se traduit pas que par les insultes, les sifflements et les regards lubriques mais peut aussi passer par les attouchements, l’exhibition sexuelle et amener à l’agression sexuelle ou au viol. Hors, selon les chiffres de l’HCEfh, 16% des femmes déclarent avoir subi des viols ou tentatives de viol au cours de leur vie." +
        "Tous les comportements cités précédemment quels qu’ils soient sont passibles d’amendes ou de peines de prison (voir rubrique « Qu’est-ce que le harcèlement ? »)"
    }
];


var currentquestion = 0,
    score = 0,
    submt = true,
    picked;

jQuery(document).ready(function ($) {

    function htmlEncode(value) {
        return $(document.createElement('div')).text(value).html();
    }


    function addChoices(choices) {
        if (typeof choices !== "undefined" && $.type(choices) == "array") {
            $('#choice-block').empty();
            for (var i = 0; i < choices.length; i++) {
                $(document.createElement('li')).addClass('choice choice-box').attr('data-index', i).text(choices[i]).appendTo('#choice-block');
            }
        }
    }

    function nextQuestion() {
        $('#submitbutton').removeClass('choice-box-pro');
        submt = true;
        $('#explanation').empty();
        $('#question').text(quiz[currentquestion]['question']);
        $('#pager').text('Situation ' + Number(currentquestion + 1) + ' sur ' + quiz.length);
        if (quiz[currentquestion].hasOwnProperty('image') && quiz[currentquestion]['image'] != "") {
            if ($('#question-image').length == 0) {
                $(document.createElement('img')).addClass('question-image').attr('id', 'question-image').attr('src', quiz[currentquestion]['image']).attr('alt', htmlEncode(quiz[currentquestion]['question'])).insertAfter('#question');
            } else {
                $('#question-image').attr('src', quiz[currentquestion]['image']).attr('alt', htmlEncode(quiz[currentquestion]['question']));
            }
        } else {
            $('#question-image').remove();
        }
        addChoices(quiz[currentquestion]['choices']);
        setupButtons();


    }


    function processQuestion(choice) {

        var reponse1 = questions[currentquestion][0];
        var reponse2 = questions[currentquestion][1];
        var reponse3 = questions[currentquestion][2];
        var reponse4 = questions[currentquestion][3];

        if(choice == 0) reponse1++;
        if(choice == 1) reponse2++;
        if(choice == 2) reponse3++;
        if(choice == 3) reponse4++;

        var total = parseFloat(reponse1) + parseFloat(reponse2) + parseFloat(reponse3) + parseFloat(reponse4);
        var pourcentage;

        switch(choice) {
            case "0":
                pourcentage = (reponse1/total)*100;
                break;
            case "1":
                pourcentage = (reponse2/total)*100;
                break;
            case "2":
                pourcentage = (reponse3/total)*100;
                break;
            case "3":
                pourcentage = (reponse4/total)*100;
                break;
        }

        totalPourcentages = totalPourcentages + Math.round(pourcentage);

        if (quiz[currentquestion]['choices'][choice] == quiz[currentquestion]['correct']) {
            $('.choice').eq(choice).css({
                'background-color': '#50D943'
            });
            $('#explanation').html('<b>' + Math.round(pourcentage) + '% des personnes ont donné cette réponse.</b><br>' + htmlEncode(quiz[currentquestion]['explanation']));
            score++;
        } else {
            $('.choice').eq(choice).css({
                'background-color': '#D92623'
            });
            $('#explanation').html('<b>' + Math.round(pourcentage) + '% des personnes ont donné cette réponse.</b><br>' + htmlEncode(quiz[currentquestion]['explanation']));
        }
        currentquestion++;

        $('#submitbutton').html('Prochaine Situation &raquo;').on('click', function () {

            $.ajax({
                url: window.url + "/actualisationQuiz.php",
                type: "POST",
                data: 'question='+currentquestion+"&reponse="+choice,
                success: (function(data) {
                    $('#output').html(data);
                })
            });

            if (currentquestion == quiz.length) {
                endQuiz();
            } else {
                $(this).text('Comparer votre réponse').css({
                    'color': '#222'
                }).off('click');
                nextQuestion();
            }
        });



    }


    function setupButtons() {
        $('.choice').on('mouseover', function () {
            $(this).css({
                'background-color': '#e1e1e1'
            });
        });
        $('.choice').on('mouseout', function () {
            $(this).css({
                'background-color': '#fff'
            });
        })
        $('.choice').on('click', function () {
            picked = $(this).attr('data-index');
            $('.choice').removeAttr('style').off('mouseout mouseover');
            $(this).css({
                'border-color': '#222',
                'font-weight': 700,
                'background-color': '#c1c1c1'
            });
            if (submt) {
                submt = false;
                $('#submitbutton').css({
                    'color': '#000'
                }).on('click', function () {
                    $('.choice').off('click');
                    $(this).off('click');
                    processQuestion(picked);
                });
            }
        })
    }


    function endQuiz() {
        $('#explanation').empty();
        $('#question').empty();
        $('#choice-block').empty();
        $('#submitbutton').remove();
        $('#question').text("Merci pour votre participation. Nous espérons que ce quizz vous a été utile.");
        $(document.createElement('h2')).css({
            'text-align': 'center',
            'font-size': '4em'
        }).text(Math.round(totalPourcentages / quiz.length) + '%').insertAfter('#question');
        $(document.createElement('h5')).css({
            'text-align': 'center',
            'font-size': '1em'
        }).text('Votre moyenne par rapport aux autres personnes : ').insertAfter('#question');
    }


    function init() {
        //add title
        if (typeof quiztitle !== "undefined" && $.type(quiztitle) === "string") {
            $(document.createElement('h4')).text(quiztitle).appendTo('#frame');
        } else {
            $(document.createElement('h4')).text("Quizz").appendTo('#frame');
        }

        //add pager and quiz
        if (typeof quiz !== "undefined" && $.type(quiz) === "array") {
            //add pager
            $(document.createElement('p')).addClass('pager').attr('id', 'pager').text('Situation 1 sur ' + quiz.length).appendTo('#frame');
            //add first question
            $(document.createElement('h2')).addClass('question').attr('id', 'question').text(quiz[0]['question']).appendTo('#frame');
            //add image if present
            if (quiz[0].hasOwnProperty('image') && quiz[0]['image'] != "") {
                $(document.createElement('img')).addClass('question-image').attr('id', 'question-image').attr('src', quiz[0]['image']).attr('alt', htmlEncode(quiz[0]['question'])).appendTo('#frame');
            }
            $(document.createElement('p')).addClass('explanation').attr('id', 'explanation').html('&nbsp;').appendTo('#frame');

            //quiz holder
            $(document.createElement('ul')).attr('id', 'choice-block').appendTo('#frame');

            //add choices
            addChoices(quiz[0]['choices']);

            //add submit button
            $(document.createElement('div')).addClass('choice-box').attr('id', 'submitbutton').text('Comparer votre réponse').css({
                'font-weight': 700,
                'color': '#222',
                'padding': '30px 0'
            }).appendTo('#frame');

            setupButtons();
        }
    }

    init();
});