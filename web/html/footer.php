<?php
$app = \Slim\Slim::getInstance();
$url = $app->request->getRootUri();
$url = str_replace("/index.php", "", $url);
?>
<div id="output" align="center"></div>
<script type="text/javascript" src="<?php echo $url; ?>/web/js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/web/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>/web/js/init.js"></script>
<script>
    function verifyPassword() {
        var mdp = document.getElementById('password');
        var mdp2 = document.getElementById('confirm_password');

        if (mdp.value != mdp2.value) {
            alert("Les mots de passe ne concordent pas !");
            return false;
        }
        return true;
    }
</script>
<script type="text/javascript">
    $(document).ready(
        function(){
            $('.btn-floating').click(function(){
                var offset = $('#above').offset().top
                $('html,body').animate(
                    {scrollTop: offset},'slow');
            });
        });
</script>
</body>
</html>