<?php
$app = \Slim\Slim::getInstance();
$url = $app->request->getRootUri();
$url = str_replace("/index.php", "", $url);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="<?php echo $url; ?>/web/images/favicon.ico" type="images/x-icon">
    <title>HarLESSment</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo $url; ?>/web/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo $url; ?>/web/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo $url; ?>/web/css/fonts.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body style="background-image: url(<?php echo $url; ?>/web/img/motif.png)">
