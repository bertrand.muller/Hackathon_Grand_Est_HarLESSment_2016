var express			= require('express');
var mysql			= require('mysql');
var fs				= require('fs');
var ini				= require('ini');
var config			= ini.parse(fs.readFileSync('./db.conf.ini', 'utf-8'));//./src/conf
var socketio		= require('socket.io');
var app				= express();
var Sensit			= require('sensit-api');
var sensitClient	= new Sensit({token: 'GPD35vPnwElh0j7LkAKi1aACl7upgFhMqaaiF5KsHBxJloSfXWmogeOegJ4sbzsb'});

var connection = mysql.createConnection(config.driver+'://'+config.username+':'+config.password+'@'+config.host+'/'+config.database+'?debug=false&charset='+config.charset);
connection.connect();

app.use(express.static('static'));
app.set('port', 3000);

var io = socketio.listen(app.listen(app.get('port'), function() {
	console.log('Server (http and socket.io) listening at http://dilink.ovh:'+app.get('port'));
}));

app.get('/', function(req, res) {
	res.sendFile(__dirname+'/vibrate.html');
});


io.on('connection', function(client) {
	client.on('geoloc', function(data) {
//		var lat = data.lat;
//		var lng = data.lng;
//		console.log(lat+'        '+lng);
	});
	setInterval(findLastDate, 5000);
});

var usersCountChat = 0;
io.of('/chat').on('connection', function(client) {
	usersCountChat++;
	console.log(usersCountChat);
	
	io.of('/chat').emit('users_count', {count: usersCountChat});
	client.broadcast.emit('connection');
	
	client.on('message', function(data) {
		var time = parseInt(Date.now()/1000);
		client.broadcast.emit('message', {text: data.text, time: time});
//		console.log(time);
		insertMessageInDB({idDest: 0, idExpe: 1, heure: time, message: data.text});
	});
	client.on('disconnect', function() {
		usersCountChat--;
		io.of('/chat').emit('disconnection', {count: usersCountChat});
	});
});



var secondsInterval = 12;
var lastUpdate = 0;

function findLastDate() {
	sensitClient.sensor('5044', '16246').get()
	.then(function (result) {
		var time_push = new Date(result.data.history[0].date).getTime()/1000;
		var time_now = parseInt(Date.now()/1000);
		console.log('Diff: '+(time_now - time_push)/60);
		if ((Math.abs(time_now - time_push)/60) < 1 && lastUpdate <= 0) {
			io.emit('vibrate');
			console.log('PUSHED !');
			lastUpdate = secondsInterval;
		} else {
			lastUpdate--;
			console.log(lastUpdate);
		}
	});
}



function insertMessageInDB(post, cb) {
	cb = (typeof cb=='function') ? cb : function(){};
	connection.query('INSERT INTO tchat SET ?', post, function(err, result) {
		if (err) { throw err; }
		cb();
	});
}

function destroy() {
	if (connection) {
		connection.end();
	}
}

process.on('exit', destroy);
process.on('uncaughtException', destroy);
process.on('SIGTERM', destroy);